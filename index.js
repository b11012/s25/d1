// console.log("hi");

// JSON Objects
/*
	- JSON stands for JavaScript Object Notation
	- JSON is also used in other programming languages hence the name JavaScript Object Notation
	- Core JavaScript has a built in JSON object that contains methods for parsing JSON objects and converting strings into JavaScript objects
	- JavaScript objects are not to be confused with JSON
	Syntax:
		{
			"propertyA" : "valueA",
			"propertyB" : "valueB"
		}

*/

// examples:
// {
// 	"city" : "Quezon City",
// 	"province" : "Metro Manila",
// 	"country" : "Philippines"
// }

// JSON Arrays
// "cities" : [
// 	{
// 		"city" : "Quezon City",
// 		"province" : "Metro Manila",
// 		"country" : "Philippines"
// 	},
// 	{
// 		"city" : "Makati City",
// 		"province" : "Metro Manila",
// 		"country" : "Philippines"
// 	},
// 	{
// 		"city" : "Manila City",
// 		"province" : "Metro Manila",
// 		"country" : "Philippines"
// 	}
// ]

// JSON Methods
// - The JSON object contains methods for parsing and converting data into stringified JSON

let batchesArr = [
	{
		batchName: 'Batch 182'
	},
	{
		batchName : 'Batch 183'
	}
]
// Converting Data Into Stringified JSON
// "stringify"- will convert Javascript objects into a string
/*
		- Stringified JSON is a JavaScript object converted into a string to be used in other functions of a JavaScript application
		- They are commonly used in HTTP requests where information is required to be sent and received in a stringified JSON format
		- Requests are an important part of programming where an application communicates with a backend application to perform different tasks such as getting/creating data in a database
*/
console.log('Result from stringify method:');
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
	name: 'Eren',
	age: 18,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
});
console.log(data);

// Stringify method with variables
/*
	- When information is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value with a variable
	- The "property" name and "value" would have the same name which can be confusing for beginners
	- This is done on purpose for code readability meaning when an information is stored in a variable and when the object created to be stringified is created, we supply the variable name instead of a hard coded value
	- This is commonly used when the information to be stored and sent to a backend application will come from a frontend application
	- Syntax
		JSON.stringify({
			propertyA: variableA,
			propertyB: variableB
		});
	- Since we do not have a frontend application yet, we will use the prompt method in order to gather user data to be supplied to the user details
*/

let firstName = prompt('What is your first name?');
let lastName = prompt('What is your last name?');
let age = prompt('What is your age');
let address = {
	city: prompt('Which city do you live in?'),
	country: prompt('Which country does your city belongs to?')
};

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
});
console.log(otherData);

// Converting stringified JSON into JS Objects
	// JSON.parse()
	/*
		- Objects are common data types used in applications because of the complex data structures that can be created out of them
		- Information is commonly sent to applications in stringified JSON and then converted back into objects
		- This happens both for sending information to a backend application and sending information back to a frontend application
	*/

let batchesJSON = `[
	{
		"batchName" : "Batch 125"
	},
	{
		"batchName" : "Batch 126"
	}

]`
console.log('Result from parse method:');
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{
	"name" : "Mikasa",
	"age" : 16,
	"address" : {
		"city" : "Tokyo",
		"country" : "Japan"
	}
}`

console.log(JSON.parse(stringifiedObject));
